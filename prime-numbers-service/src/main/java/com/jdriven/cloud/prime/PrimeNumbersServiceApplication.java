package com.jdriven.cloud.prime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// TODO 2.1: Enable Eureka
public class PrimeNumbersServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrimeNumbersServiceApplication.class, args);
    }
}
