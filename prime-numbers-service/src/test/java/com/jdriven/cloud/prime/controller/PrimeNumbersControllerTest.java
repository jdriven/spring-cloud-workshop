package com.jdriven.cloud.prime.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource
public class PrimeNumbersControllerTest {

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void calculatePrimeNumbers() throws Exception {
        PrimeNumbersRequest request = new PrimeNumbersRequest(0, 100);

        PrimeNumbersResponse response = testRestTemplate.postForObject("/primenumbers", request, PrimeNumbersResponse.class);

        assertThat(response, notNullValue());
        assertThat(response.getPrimeNumbers().size(), equalTo(25));
        assertThat(response.getPrimeNumbers().get(0), equalTo(2));
    }

    @Test
    public void calculatePrimeNumbersWithInvalidRequest() throws Exception {
        PrimeNumbersRequest request = new PrimeNumbersRequest(0, null);

        ResponseEntity<Map> responseEntity = testRestTemplate.postForEntity("/primenumbers", request, Map.class);

        assertThat(responseEntity.getStatusCode().value(), equalTo(400));
        assertThat((String) responseEntity.getBody().get("message"), startsWith("Validation failed for object"));
    }

}