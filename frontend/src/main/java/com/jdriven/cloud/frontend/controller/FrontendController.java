package com.jdriven.cloud.frontend.controller;

import com.jdriven.cloud.frontend.service.PrimeNumbersService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Controller
public class FrontendController {

    // TODO 1.3: Replace by PrimeServiceClient.
    private final PrimeNumbersService primeNumbersService;

    // TODO 1.3: Inject PrimeServiceClient instead.
    public FrontendController(PrimeNumbersService primeNumbersService) {
        this.primeNumbersService = primeNumbersService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String render(Model model) {
        PrimeNumbersForm form = new PrimeNumbersForm();
        form.setFrom(0);
        form.setTo(100);

        model.addAttribute("primenumbersform", form);

        return "primenumbers";
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String submitPrimenumbers(@ModelAttribute PrimeNumbersForm primenumbersform,
                                     Model model) {
        model.addAttribute("primenumbersform", primenumbersform);

        List<Integer> primeNumbers;
        String instanceId = "frontend";

        Instant start = Instant.now();

        // TODO 1.4: Replace call with a call to primeServiceClient
        primeNumbers = primeNumbersService.calculatePrimeNumbers(
            primenumbersform.getFrom(), primenumbersform.getTo()
        );

        Instant end = Instant.now();

        model.addAttribute("primenumbers", primeNumbers);
        model.addAttribute("instanceId", instanceId);
        model.addAttribute("duration", Duration.between(start, end));

        return "primenumbers";
    }
}
