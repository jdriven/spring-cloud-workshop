package com.jdriven.cloud.frontend.client;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class PrimeServiceClient {

    // TODO 4.6: Replace RestTemplate with PrimeServiceFeignClient.
    private final RestTemplate restTemplate;

    public PrimeServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    // TODO 5.3: Enable HystrixCommand
    public PrimeNumbersResponse calculatePrimeNumbers(PrimeNumbersRequest primeNumbersRequest) {
        // TODO 1.2: Call prime-numbers-service with restTemplate implementation
        // TODO 2.5: Replace hard-coded URL
        // TODO 4.6: Replace RestTemplate call with PrimeServiceFeignClient call
        return null;
    }

    // TODO 5.4: Create fallback method
}
